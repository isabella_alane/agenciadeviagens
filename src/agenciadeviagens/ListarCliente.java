/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciadeviagens;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author aluno
 */
public class ListarCliente {
    public void listar(JList listaCliente){
        try {
            //tratamento de erro, sem que a aplicacao caia
            listaCliente.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            //consulta no Banco
            String SQL = "select *from sc_Agencia_de_Viagens.Cliente";
            //Classe java responsavel para criar a instrucao
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            //guarda o resultado retornado do bancod e dados
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString(("nome")));
                listaCliente.setModel(dfm);
                c.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }           
    }
     public void listar2(JList listaViagem){
        try {
            //tratamento de erro, sem que a aplicacao caia
            listaViagem.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            //consulta no Banco
            String SQL = "select *from sc_Agencia_de_Viagens.Viagem";
            //Classe java responsavel para criar a instrucao
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            //guarda o resultado retornado do bancod e dados
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString(("destino")));
                listaViagem.setModel(dfm);
                c.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }           
    }
}
