/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciadeviagens;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
/**
 *
 * @author aluno
 */
public class Insercao {

    public void inserir(String nome, String cpf, String telefone, String rua) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_Agencia_de_Viagens.Cliente(nome, cpf, telefone,rua) values (?,?,?,?)") ;
            ps.setString(1, nome);
            ps.setString(2, cpf);
            ps.setString(3, telefone);
            ps.setString(4, rua);
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void inserir2(String cpf, String destino, String dataviagem, String dataretorno,String horario, String pacote) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_Agencia_de_Viagens.Viagem(cpf, destino, dataviagem, dataretorno, horario, pacote) values (?,?,?,?,?,?)") ;
            Date DtViagemConvertida = new SimpleDateFormat().parse(dataviagem);
            Date DtRetornoConvertida = new SimpleDateFormat().parse(dataretorno);
            Date Horario = new SimpleDateFormat().parse(horario);
            ps.setString(1, cpf);
            ps.setString(2, destino);
            ps.setDate(3, new java.sql.Date( DtViagemConvertida.getTime() ) );
            ps.setDate(4, new java.sql.Date( DtRetornoConvertida.getTime() ) );
            ps.setDate(5, new java.sql.Date( Horario.getTime() ) );
            ps.setString(6, pacote);
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void inserir3(String codigo_pas, String cpf, String naturalidade, String nacionalidade) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_Agencia_de_Viagens.Passaporte(codigo_pas, cpf, naturalidade, nacionalidade) values (?,?,?,?)") ;
            ps.setString(1, codigo_pas);
            ps.setString(2, cpf);
            ps.setString(3, naturalidade );
            ps.setString(4, nacionalidade );
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
