/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciadeviagens;

/**
 *
 * @author aluno
 */
public class Principal extends javax.swing.JFrame {

    /**
     * Creates new form PRINCIPAL
     */
    public Principal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        controleDeClientes = new javax.swing.JMenu();
        cadastro = new javax.swing.JMenuItem();
        listagem = new javax.swing.JMenuItem();
        detalhar1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        cadastroViagem = new javax.swing.JMenuItem();
        listagemViagem = new javax.swing.JMenuItem();
        detalhar2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 300));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -20, 400, 300));

        controleDeClientes.setText("Cliente");

        cadastro.setText("Cadastro");
        cadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroActionPerformed(evt);
            }
        });
        controleDeClientes.add(cadastro);

        listagem.setText("Listagem");
        listagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listagemActionPerformed(evt);
            }
        });
        controleDeClientes.add(listagem);

        detalhar1.setText("Detalhes");
        detalhar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detalhar1ActionPerformed(evt);
            }
        });
        controleDeClientes.add(detalhar1);

        jMenuBar1.add(controleDeClientes);

        jMenu2.setText("Viagens");

        cadastroViagem.setText("Cadastro");
        cadastroViagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroViagemActionPerformed(evt);
            }
        });
        jMenu2.add(cadastroViagem);

        listagemViagem.setText("Listagem");
        listagemViagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listagemViagemActionPerformed(evt);
            }
        });
        jMenu2.add(listagemViagem);

        detalhar2.setText("Detalhes");
        detalhar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detalhar2ActionPerformed(evt);
            }
        });
        jMenu2.add(detalhar2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroActionPerformed
        Cliente c = new Cliente();
        c.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        c.setVisible(true);
    }//GEN-LAST:event_cadastroActionPerformed

    private void listagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listagemActionPerformed
        ListagemCliente l = new ListagemCliente();
        l.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        l.setVisible(true);
    }//GEN-LAST:event_listagemActionPerformed

    private void cadastroViagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroViagemActionPerformed
        Viagem v = new Viagem();
        v.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        v.setVisible(true);
    }//GEN-LAST:event_cadastroViagemActionPerformed

    private void listagemViagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listagemViagemActionPerformed
        ListagemViagem lv = new ListagemViagem();
        lv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        lv.setVisible(true);
    }//GEN-LAST:event_listagemViagemActionPerformed

    private void detalhar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detalhar1ActionPerformed
        DetalharDadosCliente d = new DetalharDadosCliente();
        d.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        d.setVisible(true);
    }//GEN-LAST:event_detalhar1ActionPerformed

    private void detalhar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detalhar2ActionPerformed
        DetalharDadosViagem dv = new DetalharDadosViagem();
        dv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dv.setVisible(true);
    }//GEN-LAST:event_detalhar2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem cadastro;
    private javax.swing.JMenuItem cadastroViagem;
    private javax.swing.JMenu controleDeClientes;
    private javax.swing.JMenuItem detalhar1;
    private javax.swing.JMenuItem detalhar2;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JMenuItem listagem;
    private javax.swing.JMenuItem listagemViagem;
    // End of variables declaration//GEN-END:variables
}
